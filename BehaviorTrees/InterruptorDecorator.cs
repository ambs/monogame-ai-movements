﻿using System;
using System.Threading;

namespace BehaviorTrees
{
	public class InterruptorDecorator : Decorator
	{
		Thread childThread;
		// a tarefa filho está a ser executada?
		bool isRunning;
		// resultado que será retornado, seja definido pelo filho ou pela interrupção
		bool? result;

		public InterruptorDecorator(INode c) : base(c) { }

		public override bool Run()
		{
			result = null;
			// iniciar o filho
			childThread = new Thread(() => RunChild(child));
			childThread.Start();
			// aguardar até que haja resultados...
			try
			{
				while (!result.HasValue) Thread.Sleep(100);
			}
			catch (ThreadAbortException e)
			{

			}
			finally
			{
				Terminate();
			}


			return result.Value;
		}

		void Terminate()
		{
			if (isRunning)
			{
				childThread.Abort();
				childThread.Join();
			}
		}

		void RunChild(INode c)
		{
			isRunning = true;
			result = c.Run();
			isRunning = false;
		}

		public void SetResult(bool desiredResult)
		{
			result = desiredResult;
		}
	}
}
