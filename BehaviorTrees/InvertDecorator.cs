﻿using System;
namespace BehaviorTrees
{
	public class InvertDecorator : Decorator
	{
		public InvertDecorator(INode c) : base(c) { }

		public override bool Run()
		{
			return !child.Run();
		}
	}
}
