﻿using System;
namespace BehaviorTrees
{
	public class UntilFailDecorator : Decorator
	{
		public UntilFailDecorator(INode c) : base(c) { }

		public override bool Run()
		{
			while (true) if (!child.Run()) break;

			return true;
		}
	}
}
