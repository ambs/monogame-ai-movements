﻿using System;

namespace BehaviorTrees
{
	/// <summary>
	/// A generic node in the behavior tree.
	/// </summary>
	public interface INode
	{
		/// <summary>
		/// Run this node and descendants.
		/// <returns>success or insuccess</returns>
		/// </summary>
		bool Run();
	}
}
