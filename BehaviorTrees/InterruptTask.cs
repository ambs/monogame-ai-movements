﻿using System;
namespace BehaviorTrees
{
	public class InterruptTask : INode
	{
		
		InterruptorDecorator interruptor;

		bool desiredResult;

		public InterruptTask(InterruptorDecorator interruptor, bool result)
		{
			this.interruptor = interruptor;
			desiredResult = result;
		}

		public bool Run()
		{
			interruptor.SetResult(desiredResult);
			return true;
		}

	}

}
