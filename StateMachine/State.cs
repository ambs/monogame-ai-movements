﻿using System;
using System.Collections.Generic;

namespace StateMachines
{
	public class State
	{
		List<ITransition> transitions;
		List<Action> entryActions;
		List<Action> exitActions;
		List<Action> stateActions;

		public State(ITransition[] transitions = null,
					 Action[] entry  = null,
		             Action[] exit   = null,
		             Action[] action = null) 
		{
			this.transitions = transitions == null ?
				new List<ITransition>() :
				new List<ITransition>(transitions);
			this.entryActions = entry == null ?
				new List<Action>() :
				new List<Action>(entry);
			this.exitActions  = exit == null ?
				new List<Action>() :
				new List<Action>(exit);
			this.stateActions = action == null ?
				new List<Action>() :
				new List<Action>(action);
		}

		public void AddTransition(ITransition transition)
		{
			transitions.Add(transition);
		}
		public void AddTransitions(IEnumerable<ITransition> transitions)
		{
			this.transitions.AddRange(transitions);
		}
		public void AddEntryAction(Action action)
		{
			entryActions.Add(action);
		}
		public void AddEntryActions(IEnumerable<Action> actions)
		{
			entryActions.AddRange(actions);
		}
		public void AddExitAction(Action action)
		{
			exitActions.Add(action);
		}
		public void AddExitActions(IEnumerable<Action> actions)
		{
			exitActions.AddRange(actions);
		}
		public void AddStateAction(Action action)
		{
			stateActions.Add(action);
		}
		public void AddStateActions(IEnumerable<Action> actions)
		{
			stateActions.AddRange(actions);
		}

		public List<ITransition> GetTransitions()
		{
			return transitions;
		}
		public List<Action> GetExitAction() 
		{
			return exitActions;
		}
		public List<Action> GetAction()
		{
			return stateActions; 
		}
		public List<Action> GetEntryAction() 
		{
			return entryActions;
		}
	}
}
