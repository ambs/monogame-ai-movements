﻿using System;
using System.Collections.Generic;

namespace StateMachines
{
	public class StateMachine
	{
		State initialState;
		State currentState;

		public StateMachine(State initialState)
		{
			this.initialState = initialState;
			this.currentState = initialState;
		}

		public void Reset()
		{
			currentState = initialState;
		}

		public Func<Boolean> AndCondition(Func<Boolean> a, Func<Boolean> b)
		{
			return () => a() && b();
		}
		public Func<Boolean> OrCondition(Func<Boolean> a, Func<Boolean> b)
		{
			return () => a() || b();
		}
		public Func<Boolean> NotCondition(Func<Boolean> a)
		{
			return () => !a();
		}

		public List<Action> Update()
		{
			ITransition triggeredTransition = null;
			foreach (ITransition transition in currentState.GetTransitions())
			{
				if (transition.IsTriggered())
				{
					triggeredTransition = transition;
					break;
				}
			}

			if (triggeredTransition != null)
			{
				State targetState = triggeredTransition.GetTargetState();
				List<Action> actions = new List<Action>(currentState.GetExitAction());
				actions.AddRange(triggeredTransition.GetAction());
				actions.AddRange(targetState.GetEntryAction());
				currentState = targetState;
				return actions;
			}
			else
			{
				return currentState.GetAction();
			}
		}
	}
}
