﻿using System;
using System.Collections.Generic;

namespace StateMachines
{
	public interface ITransition
	{
		bool IsTriggered();
		State GetTargetState();
		List<Action> GetAction();
	}
}
