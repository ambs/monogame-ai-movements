﻿using System;
using System.Collections.Generic;

namespace StateMachines
{
	public class Transition : ITransition
	{
		List<Action> actions;
		State targetState;
		readonly Func<Boolean> condition;

		public Transition(Func<Boolean> condition,
		                  State target = null,
		                  List<Action> actions = null)
		{
			this.condition   = condition;
			this.targetState = target ?? null;
			this.actions     = actions ?? new List<Action>();
		}

		public void SetTargetState(State state)
		{
			targetState = state;
		}
		public void AddAction(Action action)
		{
			actions.Add(action);
		}
		public void AddActions(IEnumerable<Action> actions)
		{
			this.actions.AddRange(actions);
		}
		public List<Action> GetAction()
		{
			return actions;
		}

		public State GetTargetState()
		{
			return targetState;
		}

		public bool IsTriggered()
		{
			return condition();
		}
	}
}
