﻿using System;
using Learning.NaiveBayes;

namespace tests
{
	class MainClass
	{
		static void Main(string[] args)
		{
			NaiveBayesClassifier nb = new NaiveBayesClassifier(3);

			nb.Register(true, 0);
			nb.Register(true, 2);
			nb.Register(true, 1);
			nb.Register(false, 0);
			nb.Register(false, 0, 1);
			nb.Register(false, 0, 2);

			var Spell = nb.Predict(true, false, true);

			if (Spell) Console.WriteLine("Use Spell");
			else Console.WriteLine("No spell");
		}
	}
}
