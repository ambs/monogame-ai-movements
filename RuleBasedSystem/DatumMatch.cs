﻿using System;

namespace RuleBasedSystem
{
    public class DatumMatch<T> : Match
    {
		readonly Func<T, bool> comparer;

		public DatumMatch(string identifier) : base(identifier, false, true) { }

		public DatumMatch(string identifier, bool isWildcard, Func<T, bool> eval) : base(identifier, isWildcard)
        {
            comparer = eval;
        }

        public override bool MatchesItem(DataNode item, Bindings bindings)
        {
            if (!(typeof(Datum<T>).IsInstanceOfType(item)))
                return false;
         
            if (!IsWildcard && item.Identifier != Identifier)
                return false;

			if (IsCapturer) {
				bindings.Add(Identifier, item);
				return true;
			}

            if (comparer((item as Datum<T>).Value))
            {
				if (IsWildcard)
                	bindings.Add(Identifier, item);
                return true;
            }
            else return false;
        }
    }
}
