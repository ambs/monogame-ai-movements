﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RuleBasedSystem
{
    public class Rule
    {
        Match ifClause;
        Action<Bindings> action;

        public Rule(Match match, Action<Bindings> action)
        {
            ifClause = match;
            this.action = action;
        }

        public bool Matches(IEnumerable<DataNode> database, Bindings bindings)
        {
            return ifClause.Matches(database, bindings);
        }

        public void Run(Bindings bindings)
        {
            action(bindings);
        }
    }
}
