﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RuleBasedSystem
{
	/// <summary>
	/// A data group has an identifier and a set of childs, that can be
	/// either Datum or DataGroups.
	/// </summary>
    public class DataGroup : DataNode
    {
		public readonly IEnumerable<DataNode> Children;

		/// <summary>
		/// Initializes a new instance of the <see cref="T:RuleBasedSystem.DataGroup"/> class.
		/// </summary>
		/// <param name="identifier">Identifier.</param>
		/// <param name="child">Node data.</param>
		public DataGroup(string identifier, IEnumerable<DataNode> child) : base(identifier)
        {
            Children = child;
        }

		/// <summary>
		/// Initializes a new instance of the <see cref="T:RuleBasedSystem.DataGroup"/> class.
		/// </summary>
		/// <param name="identifier">Identifier.</param>
		/// <param name="child">Node data.</param>
        public DataGroup(string identifier, params DataNode[] child) : base(identifier)
        {
            Children = child;
        }

		/// <summary>
		/// Returns a <see cref="T:System.String"/> that represents the current <see cref="T:RuleBasedSystem.DataGroup"/>.
		/// </summary>
		/// <returns>A <see cref="T:System.String"/> that represents the current <see cref="T:RuleBasedSystem.DataGroup"/>.</returns>
        public override string ToString()
		{
			return string.Format("({0} \n{1}\n)",
					 Identifier,
                     string.Join("\n", Children.Select((arg) => arg.ToString())));
		}
    }
}
