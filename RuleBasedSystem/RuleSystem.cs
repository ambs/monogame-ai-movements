﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RuleBasedSystem
{
	public class RuleSystem
	{
		private List<DataNode> database;
		private List<Rule> rules;

		public RuleSystem()
		{
			rules = new List<Rule>();
			database = new List<DataNode>();
		}

		public void AddRules(params Rule[] rules)
		{
			this.rules.AddRange(rules);
		}


		public void Assert(params DataNode[] nodes)
		{
			this.database.AddRange(nodes);
		}

		public void Iterate()
		{
			foreach (Rule rule in rules)
			{
				Bindings bindings = new Bindings();
				if (rule.Matches(database, bindings))
				{
					rule.Run(bindings);
					return;
				}
			}
			return;
		}
	}
}
