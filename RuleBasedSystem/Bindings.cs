﻿using System.Collections.Generic;

namespace RuleBasedSystem
{
	/// <summary>
	/// Bindings is a dictionary that map names (captures or wildcards) to data.
	/// </summary>
	public class Bindings : Dictionary<string, DataNode>
    {
		/// <summary>
		/// Returns a <see cref="T:System.String"/> that represents the current <see cref="T:RuleBasedSystem.Bindings"/>.
		/// </summary>
		/// <returns>A <see cref="T:System.String"/> that represents the current <see cref="T:RuleBasedSystem.Bindings"/>.</returns>
		public override string ToString()
		{
			string s = "";
			foreach (var key in Keys)
			{
				s += key + " ~~> " + this[key];
			}
			return s;
		}
    }
}
