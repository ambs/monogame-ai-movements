﻿using System;
using System.Collections.Generic;

namespace RuleBasedSystem
{
    class OrMatch : Match
    {
        Match leftMatch, rightMatch;

        public override bool Matches(IEnumerable<DataNode> DB, Bindings bindings)
        {

            return leftMatch.Matches(DB, bindings) ||
                   rightMatch.Matches(DB, bindings);
        }

        public override bool MatchesItem(DataNode item, Bindings bindings)
        {
			return leftMatch.MatchesItem(item, bindings) ||
				   rightMatch.MatchesItem(item, bindings);
        }
    }
}
