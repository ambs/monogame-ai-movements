﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RuleBasedSystem
{
	public abstract class DataNode
    {
		/// <summary>
		/// Gets the data node identifier.
		/// </summary>
		/// <value>The identifier.</value>
        public string Identifier { get; private set; }
        
		/// <summary>
		/// Initializes a new instance of the <see cref="T:RuleBasedSystem.DataNode"/> class.
		/// </summary>
		/// <param name="id">Data node identifier.</param>
		protected DataNode(string id)
        {
            Identifier = id;
        }
    }
}
