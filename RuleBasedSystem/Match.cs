﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RuleBasedSystem
{
    public abstract class Match
    {
        protected string Identifier;
        protected bool IsWildcard;
		protected bool IsCapturer;

        protected Match() { }
        protected Match(string identifier, bool isWildcard, bool isCapturer)
        {
            Identifier = identifier;
            IsWildcard = isWildcard;
			IsCapturer = isCapturer;

			if (IsCapturer && IsWildcard)
				throw new Exception("Capturer can't be also a wildcard");
        }
		protected Match(string identifier, bool isWildcard) : this(identifier, isWildcard, false) { }
        protected Match(string id) : this(id, false, false) { }


        public virtual bool Matches(IEnumerable<DataNode> DB, Bindings bindings)
        {
            foreach (DataNode item in DB)
            {
                // comparar
                if (MatchesItem(item, bindings))
                    return true;                
            }
            // não encontrado
            return false;
        }

        public abstract bool MatchesItem(DataNode item, Bindings bindings);
    }
}
