﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RuleBasedSystem
{
    public class DataGroupMatch : Match
    {
        IEnumerable<Match> childMatchs;

		public DataGroupMatch(string identifier, bool isWildcard, params Match[] childs)
			: base(identifier, isWildcard)
		{
			childMatchs = childs;
		}

		public DataGroupMatch(string identifier) : base(identifier, false, true) { }


		public override bool MatchesItem(DataNode item, Bindings bindings)
        {
			
			if (!(typeof(DataGroup).IsInstanceOfType(item)))
                return false;


            if (!IsWildcard && Identifier != item.Identifier)
                return false;

			if (IsCapturer)
			{
				bindings.Add(Identifier, item);
				return true;
			}

            foreach (Match match in childMatchs)
                if (!match.Matches((item as DataGroup).Children, bindings))
                    return false;
			
            if (IsWildcard)
                bindings.Add(Identifier, item);
			
            return true;
        }


    }
}
