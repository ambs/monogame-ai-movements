﻿using System;
using System.Collections.Generic;

namespace RuleBasedSystem
{
	/// <summary>
	/// Given two Match objects, create their conjunction (and).
	/// </summary>
    class AndMatch : Match
    {
  		readonly Match rightMatch, leftMatch;

		/// <summary>
		/// Creates a new Match based on the conjunction of two submatches.
		/// </summary>
		/// <param name="l">The left match.</param>
		/// <param name="r">The right match.</param>
		public AndMatch(Match l, Match r)
		{
			leftMatch = l;
			rightMatch = r;
		}
        
		/// <summary>
		/// Matches against the fact database, populating Bindings accordingly.
		/// </summary>
		/// <param name="DB">Facts database.</param>
		/// <param name="bindings">Output bindings.</param>
        public override bool Matches(IEnumerable<DataNode> DB, Bindings bindings)
        {
            return leftMatch.Matches(DB, bindings) &&
                   rightMatch.Matches(DB, bindings);
        }

		/// <summary>
		/// Matches an item and creates bindings.
		/// </summary>
		/// <returns><c>true</c>, if item was matched, <c>false</c> otherwise.</returns>
		/// <param name="item">Database fact.</param>
		/// <param name="bindings">Bindings.</param>
        public override bool MatchesItem(DataNode item, Bindings bindings)
        {
			return leftMatch.MatchesItem(item, bindings) &&
				   rightMatch.MatchesItem(item, bindings);
        }
    }
}
