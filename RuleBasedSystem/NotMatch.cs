﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RuleBasedSystem
{
    class NotMatch : Match
    {
        Match match;
        public override bool Matches(IEnumerable<DataNode> DB, Bindings bindings)
        {
			return !match.Matches(DB, new Bindings());
        }

        public override bool MatchesItem(DataNode item, Bindings bindings)
        {
			return !match.MatchesItem(item, new Bindings());
        }
    }
}
