﻿namespace RuleBasedSystem
{
    public class Datum<T> : DataNode
    {
		/// <summary>
		/// Datum value acessor.
		/// </summary>
		/// <value>The value.</value>
        public T Value { private set; get; }

		/// <summary>
		/// Initializes a new instance of the <see cref="T:RuleBasedSystem.Datum`1"/> class.
		/// </summary>
		/// <param name="id">Datum identifier.</param>
		/// <param name="val">Datum value.</param>
        public Datum(string id, T val) : base(id)
        {
            Value = val;
        }

		/// <summary>
		/// Returns a <see cref="T:System.String"/> that represents the current <see cref="T:RuleBasedSystem.Datum`1"/>.
		/// </summary>
		/// <returns>A <see cref="T:System.String"/> that represents the current <see cref="T:RuleBasedSystem.Datum`1"/>.</returns>
		public override string ToString()
		{
			return string.Format("({0} {1})", Identifier, Value);
		}
    }
}
