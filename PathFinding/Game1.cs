﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace PathFinding
{
	public class Game1 : Game
	{
		GraphicsDeviceManager graphics;
		SpriteBatch spriteBatch;
		Texture2D spriteSheet;
		int[,] map;
		public static int tileWidth = 16;
		Guy guy;

		public class Connection
		{
			public	Vector2 from;
			public Vector2 to;
			public int cost;
			public Connection(Vector2 o, Vector2 t)
			{
				from = o; to = t; cost = 1;
			}
		}

		bool isWalkable(float x, float y)
		{
			return isWalkable((int)x, (int)y);
		}

		List<Connection> getConnections(Vector2 node)
		{
			List<Connection> l = new List<Connection>();

			if (isWalkable(node.X - 1, node.Y))
				// path left
				l.Add(new Connection(node,node - Vector2.UnitX));
			if (isWalkable(node.X + 1, node.Y))
				// path right
				l.Add(new Connection(node,node + Vector2.UnitX));
			if (isWalkable(node.X, node.Y - 1))
				// path up
				l.Add(new Connection(node,node - Vector2.UnitY));
			if (isWalkable(node.X, node.Y + 1))
				// pathDown
				l.Add(new Connection(node,node + Vector2.UnitY));

			return l;
		}

		public Game1()
		{
			graphics = new GraphicsDeviceManager(this);
			Content.RootDirectory = "Content";
		}

		protected override void Initialize()
		{
			graphics.IsFullScreen = false;
			graphics.PreferredBackBufferWidth = 800;
			graphics.PreferredBackBufferHeight = 800;
			graphics.ApplyChanges();

			base.Initialize();
		}

		protected override void LoadContent()
		{
			spriteBatch = new SpriteBatch(GraphicsDevice);
			spriteSheet = Content.Load<Texture2D>("cobbleset-64");
			guy = new Guy(Content);


			LoadMap();

			List<Vector2> path = null;
			var watch = System.Diagnostics.Stopwatch.StartNew();
			//	List<Vector2> path = Dijkstra(2 * Vector2.One, 47 * Vector2.One);

			for (int i = 0; i < 100; i++)
			{
				path = Dijkstra(2 * Vector2.One, 47 * Vector2.One);
			}
			watch.Stop();
			var elapsedMs = watch.ElapsedMilliseconds;
			Console.WriteLine("Astar took " + ((double)elapsedMs/100) + " milliseconds");


			guy.Walk(path.ToArray());


		}



		protected override void Update(GameTime gameTime)
		{
			if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
				Exit();
			guy.Update(gameTime);
			base.Update(gameTime);
		}

		protected override void Draw(GameTime gameTime)
		{
			graphics.GraphicsDevice.Clear(Color.CornflowerBlue);

			spriteBatch.Begin();
			DrawBoard();
			guy.Draw(spriteBatch);
			spriteBatch.End();

			base.Draw(gameTime);
		}

		private void DrawBoard()
		{
			for (int x = 0; x < 50; x++)
			{
				for (int y = 0; y < 50; y++)
				{
					Point size = new Point(tileWidth, tileWidth);
					int l = (map[x, y] - 1) % tileWidth;
					Point target = new Point(l * tileWidth, map[x, y] - 1 - l);



					spriteBatch.Draw(spriteSheet, new Vector2(x, y) * tileWidth,
									 new Rectangle(target, size),
					                 Color.White, 0, Vector2.Zero, Vector2.One,
									 SpriteEffects.None, 0);
				}
			}
		}
		private void LoadMap()
		{
			map = new int[50, 50];
			System.IO.StreamReader file = new System.IO.StreamReader("Content/maze.tmx");
			string line;
			int y = 0;
			while ((line = file.ReadLine()) != null)
			{
				if (Regex.Matches(line, "\\s*<").Count != 0) continue;
				int x = 0;

				foreach (int cell in line.Split(',').TakeWhile((arg) => arg.Length > 0).Select<string, int>((arg) => int.Parse(arg)))
				{
					map[x++, y] = cell;
				}
				y++;
			}
			file.Close();
		}

		private bool isWalkable(int x, int y)
		{
			return map[x, y] == 45;
		}

		class NodeRecord
		{
			public Vector2 node;
			public Connection connection;
			public int costSofar;
		}

		class AstarNodeRecord
		{
			public Vector2 node;
			public Connection connection;
			public float costSofar;
			public float estimatedTotalCost;
		}

		List<Vector2> Dijkstra(Vector2 start, Vector2 end)
		{
			List<NodeRecord> open = new List<NodeRecord>();
			List<NodeRecord> closed = new List<NodeRecord>();

			NodeRecord startRecord = new NodeRecord();
			startRecord.node = start;
			startRecord.connection = null;
			startRecord.costSofar = 0;

			open.Add(startRecord);

			//

			NodeRecord current =null;
			while (open.Count > 0)
			{
				// procurar nodo com menor custo
				current = open.OrderBy((n) => n.costSofar).First();
				// is this over yet?
				if (current.node == end) break;
				// obter conexoes
				List<Connection> connections = getConnections(current.node);
				// foreach connection
				foreach (var connection in connections)
				{
					Vector2 endNode = connection.to;
					// Ja processamos este nodo?
					if (closed.Exists(obj => obj.node == endNode))
						continue;
					// calcular custo acumulado desta conexao
					int endNodeCost = current.costSofar + connection.cost;

					NodeRecord endNodeRecord = null;
					// Encontrmaos na openList?
					bool wasFound = false;
					endNodeRecord = open.Find(a => a.node == endNode);
					if (endNodeRecord != null)
					{
						// esta na lista
						wasFound = true;
						if (endNodeRecord.costSofar <= endNodeCost)
							continue;
					}
					else 
					{
						endNodeRecord = new NodeRecord();
						endNodeRecord.node = endNode;
					}
					endNodeRecord.costSofar = endNodeCost;
					endNodeRecord.connection = connection;
					if (!wasFound) open.Add(endNodeRecord);
				}
				open.Remove(current);
				closed.Add(current);
			}

			if (current.node != end) return null;

			List<Connection> path = new List<Connection>();
			while (current.node != start)
			{
				path.Add(current.connection);
				current = closed.Find(r => r.node == current.connection.from);
			}
			path.Reverse();

			List<Vector2> points = path.Select<Connection, Vector2>(c => c.from).
                                   ToList();
			points.Add(end);
			return points;
		}


		List<Vector2> Astar(Vector2 start, Vector2 end)
		{
			// Criar nova heuristica
			Heuristic heuristic = new Heuristic(end);

			List<AstarNodeRecord> open   = new List<AstarNodeRecord>();
			List<AstarNodeRecord> closed = new List<AstarNodeRecord>();

			AstarNodeRecord startRecord = new AstarNodeRecord();
			startRecord.node = start;
			startRecord.connection = null;
			startRecord.costSofar = 0f;
			startRecord.estimatedTotalCost = heuristic.GetEstimatedCost(start);

			open.Add(startRecord);

			// ---

			AstarNodeRecord current = null;
			while (open.Count > 0)
			{
				// procurar nodo com menor custo
				current = open.OrderBy((n) => n.estimatedTotalCost).First();
				// is this over yet?
				if (current.node == end) break;
				// obter conexoes
				List<Connection> connections = getConnections(current.node);

				// ---

				// foreach connection
				foreach (var connection in connections)
				{
					Vector2 endNode = connection.to;
					// calcular custo acumulado desta conexao
					float endNodeCost = current.costSofar + connection.cost;

					bool wasFound = false;
					float endNodeHeuristic = 0; //// <-----
					// Ja processamos este nodo?
					AstarNodeRecord endNodeRecord = null;
					endNodeRecord = closed.Find(x => x.node == endNode);
					if (endNodeRecord != null)
					{
						// quando visitamos este nodo o custo era menor?
						if (endNodeRecord.costSofar < endNodeCost) continue;
						// nop, estamos num percurso mais curto. Reabrir nodo
						closed.Remove(endNodeRecord);
						endNodeHeuristic = endNodeRecord.estimatedTotalCost -
														endNodeRecord.costSofar;
					}

					// ---

					else if (
						(endNodeRecord = open.Find(a => a.node == endNode))
						     !=null) {
						// Encontrmaos na openList
						wasFound = true;
						if (endNodeRecord.costSofar <= endNodeCost)
							continue;
						endNodeHeuristic = endNodeRecord.estimatedTotalCost -
														endNodeRecord.costSofar;
					}

					// ----


					else
					{
						endNodeRecord = new AstarNodeRecord();
						endNodeRecord.node = endNode;
						endNodeHeuristic = heuristic.GetEstimatedCost(endNode);
					}
					endNodeRecord.costSofar = endNodeCost;
					endNodeRecord.connection = connection;
					endNodeRecord.estimatedTotalCost = endNodeCost +
															endNodeHeuristic;
					if (!wasFound) open.Add(endNodeRecord);
				}
				open.Remove(current);
				closed.Add(current);
			}

			if (current.node != end) return null;

			List<Connection> path = new List<Connection>();
			while (current.node != start)
			{
				path.Add(current.connection);
				current = closed.Find(r => r.node == current.connection.from);
			}
			path.Reverse();

			List<Vector2> points = path.Select<Connection, Vector2>(c => c.from).
								   ToList();
			points.Add(end);
			return points;
		}
	}
}








