﻿using System;
using Microsoft.Xna.Framework;

namespace PathFinding
{
	// Euclidean distance heuristic
	public class Heuristic
	{
		Vector2 end;
		public Heuristic(Vector2 endNode)
		{
			end = endNode;
		}

		public float GetEstimatedCost(Vector2 startNode)
		{
			return (end - startNode).Length();
		}
	}
}
