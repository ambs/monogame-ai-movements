﻿using System;

namespace DecisionTrees
{
	/// <summary>
	/// DTAction represents a leaf in a decision tree, and encapsulates
	/// the code to be run when this decision is taken.
	/// </summary>
	public class DTAction : DTNode
	{
		readonly Action action;

		/// <summary>
		/// Initializes a new instance of the <see cref="T:DecisionTrees.DTAction"/> class.
		/// </summary>
		/// <param name="action">An Action object, repesenting the action to take.</param>
		public DTAction(Action action)
		{
			this.action = action;
		}

		public override DTAction MakeDecision()
		{
			return this;
		}

		public void Run()
		{
			this.action();
		}
	}
}