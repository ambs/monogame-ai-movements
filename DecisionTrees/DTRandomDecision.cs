﻿using System;
namespace DecisionTrees
{
	public class DTRandomDecision : DTNode
	{
		readonly DTNode[] actions;
		readonly Random rng;

		/// <summary>
		/// Initializes a new instance of the <see cref="T:DecisionTrees.DTRandomDecision"/> class.
		/// </summary>
		/// <param name="child">An array of sub-trees to be chosen randomly from.</param>
		public DTRandomDecision(DTNode[] child)
		{
			this.actions = child;
			rng = new Random();
		}


		public override DTAction MakeDecision()
		{
			return actions[rng.Next(actions.Length)].MakeDecision();
		}
	}
}
