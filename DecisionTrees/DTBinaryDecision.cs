﻿using System;
namespace DecisionTrees
{
	public class DTBinaryDecision : DTNode
	{
		readonly Func<Boolean> testFunction;
		readonly DTNode trueTree, falseTree;

		/// <summary>
		/// Initializes a new instance of the <see cref="T:DecisionTrees.DTBinaryDecision"/> class.
		/// </summary>
		/// <param name="test">The boolean test, as a function returning a boolean.
		/// When possible use it as a closure.</param>
		/// <param name="tTree">Sub-decision tree to be evaluated when the test holds a true value.</param>
		/// <param name="fTree">Sub-decision tree to be evaluated when the test holds a false value.</param>
		public DTBinaryDecision(Func<Boolean> test, DTNode tTree, DTNode fTree) 
		{
			testFunction = test;
			trueTree = tTree;
			falseTree = fTree;
		}

		public override DTAction MakeDecision()
		{
			bool value = testFunction();
			return (value ? trueTree : falseTree).MakeDecision();
		}
	}
}
