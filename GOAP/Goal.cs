﻿using System;
namespace GOAP
{
	public class Goal
	{
		public string Name { get; private set; }
		public float Value { get; private set; }
		float change;

		public Goal(string n) : this(n, 0, 0) { }
		public Goal(string n, float v, float inc)
		{
			Name = n;
			Value = v;
			change = inc;
		}

		// incremento de insistência por unidade de tempo
		public float GetChange()
		{   // FIXME: Make this dynamic.
			return change;
		}
	}
}
