﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace GOAP
{
	/// <summary>
	/// goal-oriented action planning
	/// </summary>
	public class GOAPSystem
	{
		IEnumerable<Goal> goals;
		IEnumerable<GOAPAction> actions;



		public GOAPSystem(IEnumerable<Goal> goals, IEnumerable<GOAPAction> actions)
		{
			this.goals = goals;
			this.actions = actions;
		}

		public void ExecuteAction(string action)
		{
			// fixme
		}

		public GOAPAction ChooseAction()
		{
			// percorrer cada ação e calcular descontentamento
			GOAPAction bestAction = actions.First();
			float bestValue = CalculateDiscontentment(bestAction, goals);
			foreach (GOAPAction action in actions.Skip(1))
			{
				float thisValue = CalculateDiscontentment(action, goals);
				if (thisValue < bestValue)
				{
					bestValue = thisValue;
					bestAction = action;
				}
			}

			return bestAction;
		}



		private float CalculateDiscontentment(GOAPAction action, IEnumerable<Goal> goals)
		{
			// inicializar acumulador
			float discontentment = 0f;
			// percorrer todos os objetivos
			foreach (Goal goal in goals)
			{
				// calcular insistencia apos acao
				float newValue = goal.Value + action.GetGoalChange(goal.Name);
				// calcular alteração ao longo do tempo
				newValue += action.GetDuration() * goal.GetChange();
				// acumular descontentamento
				discontentment += newValue * newValue;
			}
			return discontentment;
		}


	}
}
