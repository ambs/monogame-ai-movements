﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace GOAP
{
	public class GOAPAction
	{
		Dictionary<string, float> goalChanges;
		float duration;

		public GOAPAction(float duration,
		                  IEnumerable<Tuple<string, float>> goalChanges)
		{
			this.duration = duration;
			this.goalChanges = new Dictionary<string, float>();
			foreach (var gc in goalChanges)
			{
				this.goalChanges[gc.Item1] = gc.Item2;
			}
		}

		public float GetGoalChange(string goal)
		{
			return goalChanges.ContainsKey(goal) ? goalChanges[goal] : 0f;
		}

		public float GetDuration() // FIXME: make this dynamic.
		{
			return duration;
		}

		public override string ToString()
		{
			string txt = "";
			txt = String.Join(",", goalChanges.Select(
				(arg1) => string.Format("{0}:{1}", arg1.Key, arg1.Value)));
			return string.Format("[GOAPAction takes {0} for {1}]", duration, txt);
		}
	}
}
