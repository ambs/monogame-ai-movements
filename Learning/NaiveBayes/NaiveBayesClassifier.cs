﻿using System;
namespace Learning.NaiveBayes
{
	public class NaiveBayesClassifier
	{
		// Nr de vezes que ação foi despoletada
		int exampleCountPositive = 0;
		// Nr de vezes que ação não foi despoletada
		int exampleCountNegative = 0;

		// contagem de atributos verdadeiros quando a ação foi
		// despoletada
		int[] attributeCountsPositive;
		// contagem de atributos verdadeiros quando a ação não foi
		// despoletada
		int[] attributeCountsNegative;

		// número de atributos observados
		int nrAttributes;

		public NaiveBayesClassifier(int nrAttributes)
		{
			this.nrAttributes = nrAttributes;
			attributeCountsNegative = new int[nrAttributes];
			attributeCountsPositive = new int[nrAttributes];
		}

		// action = True, regista evento com ação despoletada
		// action = False, regista evento com ação ñ despoletada
		// atributos: indices dos atributos a true (que ocorreram)
		public void Register(bool action, params int[] attributes)
		{
			if (action)
			{
				// Registar ação despoletada
				exampleCountPositive++;
				foreach (var attr in attributes)
					attributeCountsPositive[attr]++;
			}
			else
			{
				// Registar ação não despoletada
				exampleCountNegative++;
				foreach (var attr in attributes)
					attributeCountsNegative[attr]++;
			}
		}

		public bool Predict(params bool[] attributes)
		{
			var pv = NaiveProbabilities(attributes,
										attributeCountsPositive,
										exampleCountPositive,
										exampleCountNegative);
			
			var pf = NaiveProbabilities(attributes,
			                            attributeCountsNegative,
			                            exampleCountNegative,
			                            exampleCountPositive);
			return pv >= pf;
		}

		double NaiveProbabilities(bool[] attributes, 
		                          int[] attributeCounts, 
		                          int countObs, int countNotObs)
		{
			// nr de vezes que foi observado, sobre total de observacoes
			double prior = (double)countObs / (countObs + countNotObs);
			double p = 1.0;
			for (int i = 0; i < nrAttributes; i++)
			{
				p = p / countObs;
				if (attributes[i])
				{
					p = p * attributeCounts[i];
				}
				else
				{
					p = p * (countObs - attributeCounts[i]);
				}
			}
			return p * prior;
		}
}
}
