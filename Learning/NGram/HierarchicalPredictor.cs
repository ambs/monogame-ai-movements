﻿using System;
namespace Learning.NGram
{
	public class HierarchicalPredictor
	{
		Predictor[] ngrams;
		readonly int nValue;
		readonly int theresold;

		public HierarchicalPredictor(int n)
		{
			nValue = n;
			ngrams = new Predictor[n - 1];
			for (int i = 0; i < n - 1; i++)
				ngrams[i] = new Predictor(i + 1);
		}

		public void RegisterSequence(string actions)
		{
			if (actions.Length > nValue)
				throw new Exception("RegisterSequence: too large sequence");
			int delta = nValue - actions.Length;

			for (int i = 0; i < nValue - 1; i++)
			{
				if (i >= delta)
				{
					string subActions = actions.Substring(i - delta,
														  actions.Length - i);
					ngrams[nValue - 1 - i].RegisterSequence(subActions);
				}
			}
		}

		public string GetMostLikely(string actions)
		{
			if (actions.Length > nValue - 1) 
				throw new Exception("GetMostLikely: too large sequence");

			int delta = nValue - actions.Length;

			for (int i = 0; i < nValue - 1; i++)
			{
				if (i >= delta)
				{
					Predictor ngram = ngrams[i];
					string subActions = actions.Substring(i - delta,
									  actions.Length - i);
					if (ngram.Contains(subActions) && ngram.SequenceCount(subActions) > theresold)
					{
						return ngram.GetMostLikely(subActions);
					}
				}
			}
			return null;
		}
	}
}
