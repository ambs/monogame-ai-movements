﻿using System;
using System.Collections.Generic;

namespace Learning.NGram
{
	/// <summary>
	/// Simple NGram predictor. Uses a string to represent a sequence of
	/// actions (one character per action).
	/// </summary>
	public class Predictor
	{
		readonly int nValue;
		readonly Dictionary<string, KeyDataRecord> data;

		/// <summary>
		/// Initializes a new instance of the <see cref="T:Learning.NGram.Predictor"/> class
		/// with a prediction window of <paramref name="n"/>-1 actions.
		/// </summary>
		/// <param name="n">Size of the n-gram predictor.</param>
		public Predictor(int n)
		{
			nValue = n;
			data = new Dictionary<string, KeyDataRecord>();
		}

		public void RegisterSequence(string actions)
		{
			if (actions.Length != nValue)
				throw new Exception("RegisterSequence: invalid sequence size");

			string key = actions.Substring(0, nValue - 1);
			string value = actions.Substring(nValue - 1);

			if (!data.ContainsKey(key))
				data[key] = new KeyDataRecord();
			KeyDataRecord keyData = data[key];
			if (!keyData.counts.ContainsKey(value))
				keyData.counts[value] = 0;
			keyData.counts[value]++;
			keyData.total++;
		}

		public bool Contains(string actions)
		{
			return data.ContainsKey(actions);
		}
		public int SequenceCount(string actions)
		{
			return data[actions].total;
		}

		public string GetMostLikely(string actions)
		{
			if (actions.Length != nValue - 1)
				throw new Exception("GetMostLikely: invalid sequence size");
			if (!data.ContainsKey(actions))
				return null;

			KeyDataRecord keyData = data[actions];
			int highestValue = 0;
			string bestAction = null;

			foreach (var action in keyData.counts.Keys)
			{
				if (keyData.counts[action] > highestValue)
				{
					highestValue = keyData.counts[action];
					bestAction = action;
				}
			}
			return bestAction;
		}
	}

	/// <summary>
	/// Counts for each predicted action
	/// </summary>
	class KeyDataRecord
	{
		public Dictionary<string, int> counts;
		public int total;

		public KeyDataRecord()
		{
			counts = new Dictionary<string, int>();
			total = 0;
		}
	}
}
