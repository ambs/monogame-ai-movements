﻿using System;
using Microsoft.Xna.Framework.Graphics;

namespace AI
{
	public interface IRayCast
	{
		bool RayCollides(Ray ray, out Collision collision);
		void Draw(SpriteBatch s);
	}
}
