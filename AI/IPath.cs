﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace AI
{
	public interface IPath
	{
		Vector2 GetPosition(float param);
		float GetParam(Vector2 pos, float lastParam);
		IList<Vector2> GetPoints();
		float MaxParam();
	}
}

