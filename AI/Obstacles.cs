﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework.Graphics;

namespace AI
{
	public static class Obstacles
	{
		static List<IRayCast> colliders;
		static Obstacles()
		{
			colliders = new List<IRayCast>();
		}

		public static void Draw(SpriteBatch s)
		{
			foreach (IRayCast collider in colliders)
				collider.Draw(s);
		}

		public static void RegisterObstacle(IRayCast collider)
		{
			colliders.Add(collider);
		}

		public static bool getCollision(Ray ray, out Collision collision)
		{
			bool collided = false;
			collision = new Collision();
			foreach (IRayCast collider in colliders)
			{
				Collision c;
				if (collider.RayCollides(ray, out c))
					if (collided &&
						(ray.Origin - c.point).LengthSquared() < (ray.Origin - collision.point).LengthSquared())
					{
						collision = c;
					}
					else {
						collided = true;
						collision = c;
					}
			}
			return collided;
		}
	}
}
