﻿using System;
using Microsoft.Xna.Framework;

namespace AI
{
	public struct Collision
	{
		public Vector2 point;
		public Vector2 normal;
	}
}
