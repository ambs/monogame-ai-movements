﻿using System;
using Microsoft.Xna.Framework;

namespace AI
{
	public static class GameTimeHelper
	{
		public static float deltaTime(this GameTime self)
		{
			return (float)self.ElapsedGameTime.TotalSeconds;
		}
	}
}

