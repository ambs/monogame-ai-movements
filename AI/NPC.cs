﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace AI
{
	public class NPC : Sprite
	{
		float maxSpeed = 200f;
		float maxAccell = 20f;

		Behavior behavior;

		public NPC(ContentManager Content, Vector2 pos) : base (Content, "worm1", pos)
		{
			info.velocity = Vector2.Zero;
			info.rotation = 0f;
		}

		public void SetBehavior(Behavior beh)
		{
			behavior = beh;
		}


		public override void Draw(SpriteBatch sb, GameTime gametime)
		{
			if (behavior != null)
				behavior.Draw(sb, gametime);
			
			base.Draw(sb, gametime);
		}

		public override void Update(GameTime gametime)
		{
			info.position    += info.velocity * gametime.deltaTime();
			info.orientation += info.rotation * gametime.deltaTime();

			info.velocity *= 0.9f;
			info.rotation *= 0.9f;

			if (behavior != null)
			{
				Steering steering = behavior.getSteering();
				info.velocity += steering.linear;
				info.rotation += steering.angular;

				if (info.velocity.LengthSquared() > maxSpeed * maxSpeed)
				{
					info.velocity.Normalize();
					info.velocity *= maxSpeed;
				}
			}
			base.Update(gametime);
		}
	}
}

