﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace AI
{
	public static class SpriteBatchHelper
	{
		static Texture2D pixel;

		static void _create_pixel(SpriteBatch sb)
		{
			pixel = new Texture2D(sb.GraphicsDevice, 1, 1);
			pixel.SetData<Color>(new Color[1] { new Color(Color.White, 1) });
		}

		static void _DrawPixel(this SpriteBatch sb, float x, float y, Color c)
		{
			sb.Draw(pixel, new Vector2(x, y), c);
		}

		public static void DrawPolyLine(this SpriteBatch sb, IList<Vector2> points, Color c)
		{
			if (pixel == null) _create_pixel(sb);

			for (int i = 0; i < points.Count - 1; i++)
			{
				sb._DrawLine(points[i], points[i + 1], c);
			}
		}

		static void _swap(ref Vector2 a, ref Vector2 b)
		{
			float t;
			t = a.X; a.X = b.X; b.X = t;
			t = a.Y; a.Y = b.Y; b.Y = t;
		}

		static void _DrawLine(this SpriteBatch sb, Vector2 f, Vector2 t, Color c)
		{
			float a = (t.Y - f.Y) / (t.X - f.X);
			float b = t.Y - a * t.X;

			if ((int)f.X == (int)t.X || (int)t.Y == (int)f.Y)
			{
				if (f.X > t.X) _swap(ref f, ref t);
				if (f.Y > t.Y) _swap(ref f, ref t);
	
				sb.Draw(pixel, _r(f.X, f.Y, t.X - f.X + 1, t.Y - f.Y + 1), c);
			}
			else if (Math.Abs(t.X - f.X) > Math.Abs(t.Y - f.Y))
			{
				if (f.X > t.X) _swap(ref f, ref t);	
				for (float x = f.X; x <= t.X; x++)
				{
					float y = a * x + b;
					sb._DrawPixel(x, y, c);
				}
			}
			else
			{
				if (f.Y > t.Y) _swap(ref f, ref t);

				for (float y = f.Y; y <= t.Y; y++)
				{
					float x = (y - b) / a;
					sb._DrawPixel(x, y, c);
				}
			}
		}

		static Rectangle _r(float a, float b, float c, float d)
		{
			return new Rectangle((int)a, (int)b, (int)c, (int)d);
		}


		public static void DrawLine(this SpriteBatch sb, Vector2 from, Vector2 to, Color color)
		{
			if (pixel == null) _create_pixel(sb);
			sb._DrawLine(from, to, color);
		}


		// Plagiarized somewhere from the internet
		public static void DrawCircle(this SpriteBatch sb,
		                       Vector2 center, float radius, Color color)
		{
			if (pixel == null) _create_pixel(sb);

			float x = radius;
			float y = 0;
			float err = 0;

			while (x >= y)
			{
				sb._DrawPixel(center.X + x, center.Y + y, color);
				sb._DrawPixel(center.X + y, center.Y + x, color);
				sb._DrawPixel(center.X - y, center.Y + x, color);
				sb._DrawPixel(center.X - x, center.Y + y, color);
				sb._DrawPixel(center.X - x, center.Y - y, color);
				sb._DrawPixel(center.X - y, center.Y - x, color);
				sb._DrawPixel(center.X + y, center.Y - x, color);
				sb._DrawPixel(center.X + x, center.Y - y, color);

				y += 1;
				err += 1 + 2 * y;
				if (2 * (err - x) + 1 > 0)
				{
					x -= 1;
					err += 1 - 2 * x;
				}
			}
		}

	}
}

