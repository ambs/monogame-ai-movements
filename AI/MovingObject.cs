﻿using System;
using Microsoft.Xna.Framework;

namespace AI
{
	public class MovingObject
	{
		protected MovementInfo info;
		public Vector2 Position() { return info.position; }
		public float Orientation() { return info.orientation; }
		public Vector2 Velocity() { return info.velocity; }
		public float Rotation() { return info.rotation; }
		public MovementInfo GetMovementInfo() { return info; }

		public MovingObject(Vector2 pos)
		{
			info.position = pos;
		}
		public MovingObject() { }
	}
}

