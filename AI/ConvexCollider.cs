﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace AI
{
	public class ConvexCollider : IRayCast
	{
		IList<Vector2> polygon;
		public ConvexCollider(IList<Vector2> points)
		{
			polygon = points;
			if (polygon[0] != polygon[polygon.Count - 1])
			{
				polygon.Add(polygon[0]);
			}
			Obstacles.RegisterObstacle(this);
		}

		protected void _swap(ref Vector2 a, ref Vector2 b)
		{
			float t;
			t = a.X; a.X = b.X; b.X = t;
			t = a.Y; a.Y = b.Y; b.Y = t;
		}

		bool RayIntersects(Ray ray, Vector2 from, Vector2 to, out Collision c)
		{
			c = new Collision();
			Vector2 P1 = ray.Origin, P2 = ray.Origin + ray.Direction, P3 = from, P4 = to;

		
			float denominator = (P4.Y - P3.Y) * (P2.X - P1.X) - (P4.X - P3.X) * (P2.Y-P1.Y);

			float numerator1 = (P4.X - P3.X) * (P1.Y - P3.Y) - (P4.Y - P3.Y) * (P1.X-P3.X);
			float numerator2 = (P2.X - P1.X) * (P1.Y - P3.Y) - (P2.Y - P1.Y) * (P1.X-P3.X);

			if (Math.Abs(denominator) <= 0.0000001f) return false;

			float ua = numerator1 / denominator;
			float ub = numerator2 / denominator;

			if (ua >= 0 && ua <= 1 && ub >= 0 && ub <= 1)
			{
				c.point = new Vector2(P1.X + ua * (P2.X - P1.X),
									  P1.Y + ua * (P2.Y - P1.Y));
				// make it point the right direction!?
				c.normal = new Vector2(P4.Y - P3.Y, -P4.X + P3.X);
				c.normal.Normalize();
				return true;
			}
			return false;
		}

		public bool RayCollides(Ray ray, out Collision collision)
		{
			bool collides = false;
			collision = new Collision();

			for (int i = 0; i < polygon.Count - 1; i++)
			{
				Collision c;
				if (RayIntersects(ray, polygon[i], polygon[i + 1], out c))
				{
					if (collides)
					{
						if ((c.point - ray.Origin).LengthSquared() <
							(collision.point - ray.Origin).LengthSquared())
						{
							collision = c;
						}
					}
					else {
						collides = true;
						collision = c;
					}
				}
			}

			return collides;
		}


		public void Draw(SpriteBatch spriteBatch)
		{
			spriteBatch.DrawPolyLine(polygon, Color.White);
		}
	}
}
