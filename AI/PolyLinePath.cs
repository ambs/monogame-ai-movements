﻿using System;
using System.Linq;
using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace AI
{
	
	public class PolyLinePath : IPath
	{
		List<Tuple<Vector2, float>> points;

		public IList<Vector2> GetPoints()
		{
			return points.Select(arg => arg.Item1).ToList<Vector2>();
		}

		public PolyLinePath()
		{
			points = new List<Tuple<Vector2, float>>();
		}

		public void Add(Vector2 point)
		{
			float distance = 0f;
			if (points.Count > 0)
			{
				Tuple<Vector2, float> last = points[points.Count - 1];
				distance = (point - last.Item1).Length() + last.Item2;
			}
			points.Add(new Tuple<Vector2, float>(point, distance));
		}

		public float GetParam(Vector2 pos, float lastParam)
		{
			if (points.Count < 2) throw new Exception("No Points on PolyLine!");
			int i = _find_segment(lastParam);
			if (i == 0) i++;
			// are we nearer than segment i-1:i or i:i+1?
			Vector2 pt = GetClosetPoint(points[i - 1].Item1, points[i].Item1, pos);
			float distance = (pt - points[i - 1].Item1).Length();
			return points[i-1].Item2 + distance;
		}

		public Vector2 GetPosition(float param)
		{
			if (points.Count < 2) throw new Exception("No Points on PolyLine!");

			int i = _find_segment(param);
			if (i == 0) return points[0].Item1;

			if (param > points[points.Count - 1].Item2)
				param = (int)points[points.Count - 1].Item2;
			// i is the second point of the segment.
			return Interpolate(points[i - 1].Item1, points[i].Item1,
							   (points[i].Item2 - points[i - 1].Item2) /
							   (points[i].Item2 - param));
		}

		public float MaxParam()
		{
			return points[points.Count - 1].Item2; 
		}

		int _find_segment(float p)
		{
			int i = 1;

			while (i < points.Count)
			{
				if (p < points[i].Item2)
					return i;
				i++;
			}
			return i - 1;
		}

		public Vector2 Interpolate(Vector2 f, Vector2 t, float p)
		{
			return Vector2.Lerp(f, t, p);
		}


		Vector2 GetClosetPoint(Vector2 A, Vector2 B, Vector2 P)
		{
			Vector2 AP = P - A;
    		Vector2 AB = B - A;
			float ab2 = AB.X * AB.X + AB.Y * AB.Y;
			float ap_ab = AP.X * AB.X + AP.Y * AB.Y;
			float t = ap_ab / ab2;
			if (t < 0.0f) t = 0.0f;
			else if (t > 1.0f) t = 1.0f;
			return A + AB * t;
		}
	}
}

