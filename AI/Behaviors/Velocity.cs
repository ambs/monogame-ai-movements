﻿using System;
namespace AI
{
	public class Velocity : Behavior
	{
		MovementInfo target;
		Sprite targetSprite;
		float maxAcceleration;
		float timeToTarget = 0.1f; // time to get to full speed

		public Velocity(Sprite self, Sprite t, float maxA) : base(self)
		{
			targetSprite = t;
			maxAcceleration = maxA;
		}

		public override Steering getSteering()
		{
			target = targetSprite.GetMovementInfo();
			// don't move if target isn't moving
			if (target.velocity.LengthSquared() <= 0.01f)
				return Steering.None();

			Steering steering = new Steering();
			// accelerate trying to get the desired velocity (target velocity)
			steering.linear = target.velocity - self.Velocity();
			steering.linear /= timeToTarget;

			// take caution not to accelerate too much
			if (steering.linear.LengthSquared() > maxAcceleration * maxAcceleration)
			{
				steering.linear.Normalize();
				steering.linear *= maxAcceleration;
			}
			steering.angular = 0f;
			return steering;
		}
	}
}
