﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace AI
{
	public class Wander : LookAt
	{
		// distância à qual o círculo de possíveis alvos é calculado
		float wanderOffset = 50f;
		// raio do círculo de possíveis alvos
		float wanderRadius = 25f;
		// rácio para mudança de direcção
		float wanderRate = (float)Math.PI / 4f;
		// orientação actual de passeio
		float wanderOrientation;
		float maxAcceleration;

		MovementInfo target;

		Random rng = new Random();

		public Wander(MovingObject s, float maxA) : base(s, s) // not really, but...
		{
			maxAcceleration = maxA;
			target = new MovementInfo();
		}

		public float randomBinomial()
		{
			return (float)(rng.NextDouble() - rng.NextDouble());
		}

		Vector2 ang2vec(float a)
		{
			return new Vector2((float)Math.Sin(a), -(float)Math.Cos(a));
		}

		public override Steering getSteering()
		{
			// calcular a orientação de passeio
			wanderOrientation += randomBinomial() * wanderRate;

			// calcular a orientação combinada para o destino
			float targetOrientation = wanderOrientation + self.Orientation();

			// calcular o centro de círculo de possíveis destinos
			target.position = self.Position() + wanderOffset * ang2vec(self.Orientation());

			// calcular a localização do destino
			target.position += wanderRadius * ang2vec(targetOrientation);

			// delegar...
			Steering steering = base.getSteering(target);

			// definir aceleração linear a toda velocidade,
			// na direcção da orientação calculada pela super classe
			steering.linear = maxAcceleration * ang2vec(self.Orientation());

			return steering;
		}

		public override void Draw(SpriteBatch sb, GameTime gametime)
		{
			sb.DrawCircle(target.position, wanderRadius, Color.Green);
		}
	}
}

