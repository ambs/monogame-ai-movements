﻿using System;
using System.Collections.Generic;

namespace AI
{
	public class WeightedBlending : Behavior
	{
		List<BehaviorAndWeight> behaviors;
		float maxAcceleration;
		float maxRotation;
		public WeightedBlending(MovingObject s,float maxA, float maxR) : base(s)
		{
			maxAcceleration = maxA;
			maxRotation = maxR;
			behaviors = new List<BehaviorAndWeight>();
		}

		public void AddBehavior(float weight, Behavior behavior)
		{
			BehaviorAndWeight bw = new BehaviorAndWeight();
			bw.weight = weight;
			bw.behavior = behavior;
			behaviors.Add(bw);
		}

		public override Steering getSteering()
		{
			Steering steering = new Steering();
			foreach (var b in behaviors)
			{
				Steering result = b.behavior.getSteering();
				steering.linear  += b.weight * result.linear;
				steering.angular += b.weight * result.angular;
			}
			if (steering.linear.Length() > maxAcceleration) {
				steering.linear.Normalize();
				steering.linear *= maxAcceleration;
			}
			steering.angular = Math.Min(steering.angular, maxRotation);
			return steering;
		}


	}
}
