﻿using System;
namespace AI
{
	public struct BehaviorAndWeight
	{
		public float weight;
		public Behavior behavior;
	}
}
