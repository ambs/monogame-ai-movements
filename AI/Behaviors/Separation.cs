﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace AI
{
	public class Separation : Behavior
	{
		float threshold = 65;
		float decayCoefficient = 1500f;
		float maxAcceleration;

		IList<MovingObject> targets;
		public Separation(MovingObject s, float m, IList<MovingObject> all) : base(s)
		{
			maxAcceleration = m;
			targets = all;
		}

		public override Steering getSteering()
		{
			Steering steering = new Steering();

			foreach (MovingObject tObj in targets)
			{
				if (tObj == self) continue;

				Vector2 direction = self.Position() - tObj.GetMovementInfo().position;
				float distance = direction.Length();
				// are we near this other guy?
				if (distance < threshold)
				{
					// compute repulsion force
					float strength = MathHelper.Min(
						decayCoefficient / (distance * distance),
						maxAcceleration);
					// add acceleration to contour this target
					direction.Normalize();
					steering.linear += strength * direction;
				}
			}
			return steering;
		}
	}
}

