﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace AI
{
	public class PredictingFollowPath : Seek
	{
		bool loop = false;
		IPath path;
		float pathOffset = 5f;
		float currentParam;
		float maxParam;
		float predictTime = .5f;

		public PredictingFollowPath(MovingObject s, float m, IPath path, bool l) : base(s,s,m)
		{
			this.path = path;
			this.currentParam = 1;
			this.maxParam = path.MaxParam();
			this.loop = l;
		}

		public override Steering getSteering()
		{
			Vector2 futurePos = self.Position() + self.Velocity() * predictTime;
			currentParam = path.GetParam(futurePos, currentParam);
			                   
			if (currentParam >= maxParam - 0.01f)
			{
				if (loop) currentParam = 0;
				else return Steering.None();
			}

			float targetParam = currentParam + pathOffset;
			MovementInfo my_target = new MovementInfo();
			my_target.position = path.GetPosition(targetParam);
			return base.getSteering(my_target);
		}

		public override void Draw(SpriteBatch sb, GameTime gametime)
		{
			sb.DrawPolyLine(path.GetPoints(), Color.Red);
			base.Draw(sb, gametime);
		}

	}
}

