﻿using System;
using Microsoft.Xna.Framework;

namespace AI
{
	public class ObstacleAvoidance : Seek
	{
		float avoidDistance = 70f;
		float lookAhead = 100f;

		public ObstacleAvoidance(MovingObject s, float ac) : base(s, null, ac)
		{
			target = new MovementInfo();
		}

		public override Steering getSteering()
		{
			Ray ray = new Ray();
			ray.Direction = self.Velocity();
			ray.Direction.Normalize();
			ray.Direction *= lookAhead;
			ray.Origin = self.Position();
			Collision collision;
			if (Obstacles.getCollision(ray, out collision))
			{
				target.position = collision.point + collision.normal * avoidDistance;
				return base.getSteering(target);
			}
			else {
				return Steering.None();
			}


		}
	}
}
