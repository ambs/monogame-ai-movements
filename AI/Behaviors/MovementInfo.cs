﻿using System;
using Microsoft.Xna.Framework;

namespace AI
{
	public struct MovementInfo
	{
		public Vector2 position;
		public Vector2 velocity;
		public float rotation;
		public float orientation;
	}
}

