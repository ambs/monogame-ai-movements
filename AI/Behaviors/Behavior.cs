﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace AI
{
	public abstract class Behavior
	{
		protected MovingObject self;
		protected Behavior(MovingObject s)
		{
			self = s;
		}
		public abstract Steering getSteering();

		public virtual void Draw(SpriteBatch sb, GameTime gametime) { }

		static protected float normAngle(float angle)
		{
			while (angle > Math.PI)
				angle -= 2f * (float)Math.PI;
			while (angle < -Math.PI)
				angle += 2f * (float)Math.PI;
			return angle;
		}

}


}

