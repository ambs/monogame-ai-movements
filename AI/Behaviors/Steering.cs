﻿using System;
using Microsoft.Xna.Framework;

namespace AI
{
	public struct Steering
	{
		public Vector2 linear;
		public float angular;

		public static Steering None()
		{
			Steering empty = new Steering();
			empty.linear = Vector2.Zero;
			empty.angular = 0f;
			return empty;
		}
	}
}
