﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace AI
{
	public class CollisionAvoidance : Behavior
	{
		float maxAcceleration;
		IList<MovingObject> targets;
		float radius = 15f;
		public CollisionAvoidance(MovingObject s, float m,
		                          IList<MovingObject> all) : base(s)
		{
			maxAcceleration = m;
			targets = all;	
		}

		public override Steering getSteering()
		{
			float shortestTime = float.MaxValue;
			MovingObject firstTarget = null;
			float firstMinSeparation = 0f;
			float firstDistance = 0f;
			Vector2 firstRelativePos = Vector2.Zero;
			Vector2 firstRelativeVel = Vector2.Zero;

			foreach (var target in targets)
			{
				if (target == self) continue;

				Vector2 relativePos = self.Position() - target.Position();
				Vector2 relativeVel = self.Velocity() - target.Velocity();
				float relativeSpeed = relativeVel.Length();
				float timeToCollision = - Vector2.Dot(relativePos, relativeVel) / (relativeSpeed * relativeSpeed);
				float distance = relativePos.Length();
				float minSeparation = distance - relativeSpeed * timeToCollision;
				if (minSeparation > 2 * radius) continue;


				if (timeToCollision > 0 && timeToCollision < shortestTime)
				{
					shortestTime = timeToCollision;
					firstTarget = target;
					firstMinSeparation = minSeparation;
					firstDistance = distance;
					firstRelativePos = relativePos;
					firstRelativeVel = relativeVel;
				}
			}

			if (firstTarget == null)
				return Steering.None();


			Vector2 _relativePos;
			Steering steering = new Steering();
			if (firstMinSeparation <= 0f || firstDistance < 2f * radius)
				_relativePos = self.Position() - firstTarget.Position();
			else
				_relativePos = firstRelativePos + firstRelativeVel * shortestTime;
			_relativePos.Normalize();
			steering.linear = _relativePos * maxAcceleration;
			return steering;
							
		}

	}
}
