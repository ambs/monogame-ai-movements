﻿using System;
using Microsoft.Xna.Framework;

namespace AI
{
	public class Arrive : Behavior
	{
		float maxAcceleration;
		float maxSpeed;

		Sprite targetSprite;
		MovementInfo target;

		float targetRadius = 20f;
		float slowRadius   = 100f;
		float timeToTarget = 0.1f;


		public Arrive(Sprite s, Sprite t, float maxA, float maxS) : base(s)
		{
			targetSprite = t;
			maxAcceleration = maxA;
			maxSpeed = maxS;
		}

		public override Steering getSteering()
		{
			Steering steering = new Steering();

			target = targetSprite.GetMovementInfo();
			// compute direction and distance to target
			Vector2 direction = target.position - self.Position();
			float distance = direction.Length();

			// are we there yet?
			if (distance < targetRadius)
			{
				return Steering.None();
			}
			// how far are us?
			float targetSpeed;
			if (distance > slowRadius)
			{
				targetSpeed = maxSpeed;
			}
			else
			{
				targetSpeed = maxSpeed * distance / slowRadius;
			}

			// compute vector to target with desired magnitude
			Vector2 targetVelocity = direction;
			targetVelocity.Normalize();
			targetVelocity *= targetSpeed;

			// how much should we accelerate to obtain desired velocity?
			steering.linear = targetVelocity - self.Velocity();
			steering.linear /= timeToTarget;

			// but never accelerate more than our engine is capable of.
			if (steering.linear.Length() > maxAcceleration)
			{
				steering.linear.Normalize();
				steering.linear *= maxAcceleration;
			}
			steering.angular = 0f;
			return steering;
		}
	}
}

