﻿using System;
namespace AI
{
	public class Flee : Behavior
	{
		float maxAcceleration;
		Sprite targetSprite;
		MovementInfo target;
		public Flee(Sprite s, Sprite t, float maxAccell) : base(s)
		{
			targetSprite = t;
			maxAcceleration = maxAccell;
		}

		public override Steering getSteering()
		{
			Steering steering = new Steering();
			target = targetSprite.GetMovementInfo();

			steering.linear = self.Position() - target.position;
			steering.linear.Normalize();
			steering.linear *= maxAcceleration;
			steering.angular = 0f;

			return steering;
		}
	}
}

