﻿using System;
using Microsoft.Xna.Framework;

namespace AI
{
	public class LookWhereYoureGoing : Align
	{
		public LookWhereYoureGoing(MovingObject s) : base(s, s) // not really, but...
		{
		}

		public override Steering getSteering()
		{
			Vector2 velocity = self.Velocity();
			if (velocity.LengthSquared() < 0.01f)
				return Steering.None();
         
			MovementInfo target = new MovementInfo();
			target.orientation = (float) Math.Atan2(-velocity.X, velocity.Y);
			return getSteering(target);
		}
	}
}

