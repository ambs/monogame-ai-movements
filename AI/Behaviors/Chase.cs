﻿using System;
using Microsoft.Xna.Framework;

namespace AI
{
	class Chase : Seek
	{
		// we will base this algorithm on Seek, so we will need
		// to save the original target so we can change it.
		MovementInfo original_target;
		float maxPrediction = 1;

		public Chase(MovingObject s, MovingObject t, float mAccel) :
			base(s, t, mAccel)
		{ }

		public override Steering getSteering()
		{
			// save original target
			target = targetSprite.GetMovementInfo();
			original_target = target;

			// compute our distance to our real target
			Vector2 direction = original_target.position - self.Position();
			float distance = direction.Length();
			// compute our current speed
			float speed = self.Velocity().Length();
			// if we are too slow to intercept, just predict the maximum allowed
			float prediction;
			if (speed <= distance / maxPrediction)
				prediction = maxPrediction;
			else
				prediction = distance / speed;

			// use the 'Seek' target and point to the interception point
			target.position += original_target.velocity * prediction;
			// call 'Seek' getSteering, that computes steering
			return getSteering(target);
		}
	}
}