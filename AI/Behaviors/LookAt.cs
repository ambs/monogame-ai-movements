﻿using System;
using Microsoft.Xna.Framework;

namespace AI
{
	public class LookAt : Align
	{
		MovementInfo original_target;
		public LookAt(MovingObject s, MovingObject o) : base(s, o)
		{
		}

		public override Steering getSteering()
		{
			// temporarilly store the original target.
			original_target = targetObject.GetMovementInfo();
			return getSteering(original_target);
		}

		public override Steering getSteering(MovementInfo original_target) {
			MovementInfo target = original_target;

			// Compute direction to target
			Vector2 direction = original_target.position - self.Position();

			// Don't move if there is not the need
			if (direction.LengthSquared() < 0.01f)
				return Steering.None();

			// Ok, we need to move, so simulate the target orientation
			target.orientation = (float) Math.Atan2(-direction.X, direction.Y);
			return base.getSteering(target);
		}
	}
}

