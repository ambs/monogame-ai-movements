﻿using System;
namespace AI
{
	public class Seek : Behavior
	{
		float maxAcceleration;
		protected MovingObject targetSprite;
		protected MovementInfo target;
		public Seek(MovingObject s, MovingObject t, float maxAccell) : base(s)
		{
			targetSprite = t;
			maxAcceleration = maxAccell;
		}

		public override Steering getSteering()
		{
			target = targetSprite.GetMovementInfo();
			return getSteering(target);
		}

		public Steering getSteering(MovementInfo target)
		{
			Steering steering = new Steering();
			steering.linear = target.position - self.Position();
			steering.linear.Normalize();
			steering.linear *= maxAcceleration;
			steering.angular = 0f;

			return steering;
		}
	}
}

