﻿using System;
using Microsoft.Xna.Framework;

namespace AI
{
	public class Align : Behavior
	{
		protected MovingObject targetObject; // object to imitate
		MovementInfo target;
		float maxAngAccel  = 0.25f; // maximum angular acceleration
		float maxRotation  = 2.0f; // maximum angular rotation speed

		float targetRadius = 0.3f; // minimum angular delta to target
		float slowRadius   = 1.0f; // angle limit for slowdown
		float timeToTarget = 0.1f; // full angular speed accel. time
		                      
		public Align(MovingObject self, MovingObject other) : base(self)
		{
			targetObject = other;
		}



		public override Steering getSteering()
		{
			target = targetObject.GetMovementInfo();
			return getSteering(target);
		}

		public virtual Steering getSteering(MovementInfo target) {
			// compute rotation distance
			float rotation = - self.Orientation() + target.orientation;
			rotation = normAngle(rotation); // make rotation lie in [-π, π]
			float rotationSize = Math.Abs(rotation); // compute absolute distance

			// are we there yet?
			if (rotationSize < targetRadius)
				return Steering.None();

			Steering steering = new Steering();
			// how far are us?
			float targetRotation = rotationSize > slowRadius ?
				maxRotation :
				maxRotation * rotationSize / slowRadius;

			// target rotation combines speed with direction
			targetRotation *= Math.Sign(rotation);
			// how much should we accelerate to obtain desired rotation?
			steering.angular = targetRotation - rotation;
			steering.angular /= timeToTarget;

			// but we can't get too fast
			float angularAcceleration = Math.Abs(steering.angular);
			if (angularAcceleration > maxAngAccel)
			{
				steering.angular /= angularAcceleration;
				steering.angular *= maxAngAccel;
			}

			steering.linear = Vector2.Zero;
			return steering;
		}
	}
}

