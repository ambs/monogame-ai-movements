﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace AI
{
	public class Ray
	{
		public Vector2 Origin;
		public Vector2 Direction;

		public void Draw(SpriteBatch s)
		{
			s.DrawCircle(Origin, 2, Color.Yellow);
			s.DrawLine(Origin, Origin + Direction, Color.GreenYellow);
		}
	}
}
