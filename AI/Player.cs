﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace AI
{
	public class Player : Sprite
	{
		float acceleration = 10f;
		float torque = (float)Math.PI/18f;
		float maxRotationVelocity = (float)Math.PI/2f;
		float angularDrag = (float)Math.PI / 180f;
		float linearDrag  = 2f;
		float maxVelocity = 300f;

		public Player(ContentManager Content, Vector2 position) : 
			base(Content, "guy", position)
		{
			info.velocity = Vector2.Zero;
			info.rotation = 0f;
		}

		public override void Update(GameTime gametime)
		{
			KeyboardState keyboard = Keyboard.GetState();

			if (keyboard.IsKeyDown(Keys.Right)) 
				info.rotation += torque;
			if (keyboard.IsKeyDown(Keys.Left))  
				info.rotation -= torque;

			info.rotation = MathHelper.Clamp(info.rotation, 
			                                 -maxRotationVelocity, maxRotationVelocity);
			info.rotation += Math.Sign(info.rotation) * (-angularDrag);
			info.orientation += info.rotation * gametime.deltaTime();

			while (info.orientation > Math.PI)  info.orientation -= (float)Math.PI * 2f;
			while (info.orientation < -Math.PI) info.orientation += (float)Math.PI * 2f;

			Vector2 accell = new Vector2(-(float)Math.Sin(info.orientation),
										  (float)Math.Cos(info.orientation)) * acceleration;

			if (keyboard.IsKeyDown(Keys.Up))   info.velocity -= accell;
			if (keyboard.IsKeyDown(Keys.Down)) info.velocity += accell;

			if (info.velocity.LengthSquared() > 0)
			{
				Vector2 drag = -info.velocity;
				drag.Normalize();
				info.velocity += drag * linearDrag;
			}

			if (info.velocity.LengthSquared() > maxVelocity * maxVelocity)
			{
				info.velocity.Normalize();
				info.velocity *= maxVelocity;
			}

			info.position += info.velocity * gametime.deltaTime();
			base.Update(gametime);
		}

	}
}

