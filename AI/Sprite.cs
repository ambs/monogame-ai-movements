﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace AI
{
	public class Sprite : MovingObject
	{
		
		Vector2 origin;

		Texture2D sprite;

		public Sprite(ContentManager content, string filename, Vector2 pos)
		{
			sprite = content.Load<Texture2D>(filename);
			info.position = pos;
			info.orientation = 0f;
			origin = new Vector2(sprite.Width, sprite.Height) * .5f;
		}

		public virtual void Update(GameTime gametime) { }

		public virtual void Draw(SpriteBatch sb, GameTime gametime)
		{
			sb.Draw(sprite, origin: origin, 
			        rotation: info.orientation, position: info.position, color: Color.White);
		}
	

	}
}

