﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace AI
{
	public class Game1 : Game
	{
		GraphicsDeviceManager graphics;
		SpriteBatch spriteBatch;
		//Random rng = new Random();
		//Player guy;
		NPC npc1;
		//List<NPC> npcs;

		bool started = false;
		//int nrNPC = 40;

		//ConvexCollider collider = new ConvexCollider(
			//new Vector2[4]{ new Vector2(200,200),
			//new Vector2(280,200), new Vector2(280,270),
			//new Vector2(200,270) }.ToList<Vector2>());




		public Game1()
		{
			graphics = new GraphicsDeviceManager(this);
			Content.RootDirectory = "Content";

		}

		protected override void Initialize()
		{
			base.Initialize();
		}

		protected override void LoadContent()
		{
			spriteBatch = new SpriteBatch(GraphicsDevice);
			//guy = new Player(Content, Vector2.One * 100);
			//npcs = new List<NPC>();

			npc1 = new NPC(Content, new Vector2(50, 50));
			WeightedBlending b1 = new WeightedBlending(npc1, 30f, 0.5f);
			MovingObject t1 = new MovingObject(new Vector2(320,320));
			b1.AddBehavior(0.15f, new Seek(npc1, t1, 30f));
			b1.AddBehavior(.5f, new ObstacleAvoidance(npc1, 30f));
			npc1.SetBehavior(b1);

			//NPC npc2 = new NPC(Content, new Vector2(200, 50));
			//WeightedBlending b2 = new WeightedBlending(npc2, 20f, 0.5f);
			//MovingObject t2 = new MovingObject(new Vector2(50, 200));
			//b2.AddBehavior(0.15f, new Seek(npc2, t2, 20f));

			//npcs.Add(npc1); npcs.Add(npc2);
			//b1.AddBehavior(.5f, new CollisionAvoidance(npc1, 20f, npcs.Cast<MovingObject>().ToList<MovingObject>()));
			//b2.AddBehavior(.5f, new CollisionAvoidance(npc2, 20f, npcs.Cast<MovingObject>().ToList<MovingObject>()));

			//npc1.SetBehavior(b1);
			//npc2.SetBehavior(b2);



		}

		protected override void Update(GameTime gameTime)
		{
			if (Keyboard.GetState().IsKeyDown(Keys.Escape))
				Exit();

			if (started)
			{
				//  guy.Update(gameTime);
					npc1.Update(gameTime);
				//for (int i = 0; i < npcs.Count; i++)
				//	npcs[i].Update(gameTime);
			}
			else if (Keyboard.GetState().IsKeyDown(Keys.Space)) {
				started = true;
			}
			base.Update(gameTime);
		}

		protected override void Draw(GameTime gameTime)
		{
			graphics.GraphicsDevice.Clear(Color.CornflowerBlue);



			spriteBatch.Begin();
			npc1.Draw(spriteBatch, gameTime);
			Obstacles.Draw(spriteBatch);

			//	guy.Draw(spriteBatch, gameTime);
			//  npc.Draw(spriteBatch, gameTime);
			//for (int i = 0; i < npcs.Count; i++)
			//	npcs[i].Draw(spriteBatch, gameTime);




			spriteBatch.End();

			base.Draw(gameTime);
		}
	}
}

